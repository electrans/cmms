import io
import os
import re
from configparser import ConfigParser
from setuptools import setup, find_packages

name = 'electrans_cmms'

ELECTRANS_MODULES = [
    'electrans_sale',
]
TRYTONSPAIN_MODULES = [
]
TRYTONZZ_MODULES = [
]
NANTIC_MODULES = [
]
ZIKZAKMEDIA_MODULES = [
]
FORZED_DEPENDENCIES = [
]


def read(fname, slice=None):
    content = io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()
    if slice:
        content = '\n'.join(content.splitlines()[slice])
    return content


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
                major_version, minor_version + 1)
    return require


config = ConfigParser()
# Take the function read_file from ConfigParser and execute it on tryton.cfg
config.read_file(open(os.path.join(os.path.dirname(__file__), 'tryton.cfg')))
# Create a dictionary with all the values of the tryton section
info = dict(config.items('tryton'))
# For each section of the tryton.cfg, create a key and within it a list
# with assigned values
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
# Pass each version number to an integer
# since then it is going to calculate the modulus
major_version = int(major_version)
minor_version = int(minor_version)

# Check the version and establish a branch to then download
# the repositories in the correct branch
series = '%s.%s' % (major_version, minor_version)
if minor_version % 2:
    branch = 'default'
else:
    branch = series

# Create two lists, one with the required repositories
# and another with the required dependencies (specified in the tryton.cfg)
requires = []
depends = info.get('depends', [])
# Add forced dependencies
if FORZED_DEPENDENCIES:
    for dependency in FORZED_DEPENDENCIES:
        if dependency not in depends:
            depends.append(dependency)
# Check the dependencies and depending on whether they are from NaN-Tic,
# TrytonSpain, ZikZakMedia or Electrans
# add the corresponding repository to requires
for dep in depends:
    if dep in NANTIC_MODULES:
        requires.append('nantic-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONSPAIN_MODULES:
        requires.append('trytonspain-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ZIKZAKMEDIA_MODULES:
        requires.append('zikzakmedia-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in TRYTONZZ_MODULES:
        requires.append('trytonzz-%(dep)s @ https://github.com/NaN-tic/'
                        'trytond-%(dep)s/archive/refs/heads/%(branch)s.zip'
                        % {'branch': branch, 'dep': dep})
    elif dep in ELECTRANS_MODULES:
        if dep == 'project_expenses':
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            'trytond-%(dep)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep})
        else:
            repo_name = dep.replace('electrans_', '')
            requires.append('%(dep)s @ https://bitbucket.org/electrans/'
                            '%(repo_name)s/get/%(branch)s.zip'
                            % {'branch': branch, 'dep': dep, 'repo_name': repo_name})
    elif not re.match(r'(ir|res)(\W|$)', dep):
        requires.append(get_require_version('trytond_%s' % dep))

requires.append(get_require_version('trytond'))

tests_require = [
    get_require_version('proteus'),
]

dependency_links = []
if minor_version % 2:
    dependency_links.append('https://trydevpi.tryton.org/')

if minor_version % 2:
    # Add development index for testing with proteus
    dependency_links.append('https://trydevpi.tryton.org/')

setup(
    # Repository name (module)
    name=name,
    # Repository version
    version=version,
    # Module description
    description='Tryton module for product manufacturer',
    # Extended description of the module,
    # in this case it will look for the README.txt
    long_description=read('README'),
    author='Electrans',
    author_email='oqueralto@electrans.com',
    # Company website
    url='http://www.electrans.com/',
    # Link referring to the repository, where it could be downloaded
    download_url="https://bitbucket.org/electrans/%s" % name,
    # It is used to specify the root directories in which
    # the project's Python packages(/trytond/modules/electrans_account_invoice)
    package_dir={'trytond.modules.electrans_cmms': '.'},
    # It is used to specify which Python packages should be
    # included in the project's distribution.
    packages=(
          ['trytond.modules.%s' % name]
          + ['trytond.modules.electrans_cmms.%s' % p for p in find_packages()]
    ),
    # It is used to specify additional files that should be included
    # in the distribution of a Python package
    package_data={
      'trytond.modules.electrans_cmms': (info.get('xml', [])
                                          + ['tryton.cfg', 'view/*.xml', 'locale/*.po', '*.fodt',
                                             'icons/*.svg', 'tests/*.rst']),
    },
    # It categorizes the repository setup, but has no implications.
    classifiers=[
      'Development Status :: 5 - Production/Stable',
      'Environment :: Plugins',
      'Framework :: Tryton',
      'Intended Audience :: Developers',
      'Intended Audience :: Financial and Insurance Industry',
      'Intended Audience :: Legal Industry',
    ],
    license='GPL-3',
    # Specifies the minimum and maximum versions of Python
    # required for the package to work
    install_requires=requires,
    dependency_links=dependency_links,
    # Is used to specify whether the Python package can be distributed
    # as a compressed (zip) file
    zip_safe=False,
    # Is used to define entry points in a Python package,
    # that allow users to run specific functions or scripts
    # from the command line
    entry_points="""
    [trytond.modules]
    electrans_cmms = trytond.modules.electrans_cmms
    """,
    test_suite='tests',
    test_loader='trytond.test_loader:Loader',
    tests_require=tests_require,
    )
