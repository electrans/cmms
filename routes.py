# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
import json
import logging

from werkzeug.exceptions import abort
from werkzeug.wrappers import Response

from trytond.transaction import Transaction
from trytond.wsgi import app
from trytond.protocols.wrappers import (
    with_pool, with_transaction, user_application, allow_null_origin)
from .schema.workpart import WorkPart, WorkLine

_logger = logging.getLogger(__name__)
cmms_application = user_application('cmms')


@app.route('/<database_name>/electrans_cmms/sale/insert', methods=['POST'])
@allow_null_origin
@with_pool
@with_transaction()
@cmms_application
def create_quotation(request, pool):
    """
    Create a new quotation from work part (GMAO)
    """

    request_body = request.get_data(as_text=True)
    data = json.loads(request_body)
    work_part = WorkPart.from_dict(data)
    if not work_part.customer_id or not work_part.project_number or not work_part.work_lines:
        return abort(422, 'Missing customer_id, project_number or work_lines')
    Party = pool.get('party.party')
    Project = pool.get('work.project')
    Sale = pool.get('sale.sale')
    SaleLine = pool.get('sale.line')
    ProductUom = pool.get('product.uom')
    User = pool.get('res.user')
    user = User(Transaction().user)
    uom = ProductUom.search([('name', '=', 'Hour')])[0]
    try:
        customer = Party(work_part.customer_id)
    except Exception:
        return abort(422, 'Invalid customer_id')
    try:
        project, = Project.search([('number', '=', work_part.project_number)])
    except Exception:
        return abort(422, 'Invalid project_number')
    with Transaction().set_context(company=user.company.id):
        sale = Sale(
            party=customer,
            project=project,
            not_from_presupuestario=True,
            is_order=False,
            lines=[
                SaleLine(
                    description=line.description,
                    unit=uom.id,
                    quantity=line.hours,
                    unit_price=1,
                    # Todo: Have to set the correct price or take a product
                ) for line in work_part.work_lines]
        )
        sale.save()
    try:
        return Response(json.dumps({'quotation_id': sale.id}), mimetype='application/json')
    except Exception:
        return abort(400, 'Error creating quotation. Please contact Tryton support')

