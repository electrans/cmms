#
# Customer Class from Antay Json schema
#
# Status = 11 -> Record active
# Status = 9 -> Record deleted
#

from dataclasses import dataclass
from typing import Optional, Any, TypeVar, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class Customer:
    business_unit_code: str
    code: str
    name: str
    status: int
    account_activity: Optional[str] = None
    account_owner_code: Optional[str] = None
    account_potentiality: Optional[int] = None
    account_size_code: Optional[str] = None
    account_state_code: Optional[str] = None
    account_type_code: Optional[str] = None
    bank_account: Optional[str] = None
    bank_name: Optional[str] = None
    blocked: Optional[int] = None
    carriage_code: Optional[str] = None
    city: Optional[str] = None
    comments: Optional[str] = None
    company_code: Optional[str] = None
    country: Optional[str] = None
    country_code: Optional[str] = None
    customer_contact: Optional[str] = None
    customer_date: Optional[str] = None
    customer_group_code: Optional[str] = None
    customer_tax_group_code: Optional[str] = None
    discount_to_apply: Optional[int] = None
    email: Optional[str] = None
    export_customer: Optional[int] = None
    fax: Optional[str] = None
    fiscal_name: Optional[str] = None
    fiscal_number: Optional[str] = None
    free_field1: Optional[str] = None
    free_field2: Optional[str] = None
    free_field3: Optional[str] = None
    free_field4: Optional[str] = None
    free_field5: Optional[str] = None
    full_address: Optional[str] = None
    is_provider: Optional[int] = None
    no_active: Optional[int] = None
    no_calculate_service_by_default: Optional[int] = None
    not_show_calculations_in_service_summary_by_default: Optional[int] = None
    payment_method_code: Optional[str] = None
    postal_code: Optional[str] = None
    potential_customer: Optional[int] = None
    price_list_code: Optional[str] = None
    print_language: Optional[str] = None
    required_documentation: Optional[int] = None
    route_code: Optional[str] = None
    schipping_method: Optional[str] = None
    special_tax: Optional[int] = None
    state_province: Optional[str] = None
    state_province_code: Optional[str] = None
    street: Optional[str] = None
    telephone1: Optional[str] = None
    telephone2: Optional[str] = None
    terms_of_payment_code: Optional[str] = None
    terms_of_payment_notes: Optional[str] = None
    web: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Customer':
        assert isinstance(obj, dict)
        business_unit_code = from_str(obj.get("BusinessUnitCode"))
        code = from_str(obj.get("Code"))
        name = from_str(obj.get("Name"))
        status = from_int(obj.get("Status"))
        account_activity = from_union([from_str, from_none], obj.get("AccountActivity"))
        account_owner_code = from_union([from_str, from_none], obj.get("AccountOwnerCode"))
        account_potentiality = from_union([from_int, from_none], obj.get("AccountPotentiality"))
        account_size_code = from_union([from_str, from_none], obj.get("AccountSizeCode"))
        account_state_code = from_union([from_str, from_none], obj.get("AccountStateCode"))
        account_type_code = from_union([from_str, from_none], obj.get("AccountTypeCode"))
        bank_account = from_union([from_str, from_none], obj.get("BankAccount"))
        bank_name = from_union([from_str, from_none], obj.get("BankName"))
        blocked = from_union([from_int, from_none], obj.get("Blocked"))
        carriage_code = from_union([from_str, from_none], obj.get("CarriageCode"))
        city = from_union([from_str, from_none], obj.get("City"))
        comments = from_union([from_str, from_none], obj.get("Comments"))
        company_code = from_union([from_str, from_none], obj.get("CompanyCode"))
        country = from_union([from_str, from_none], obj.get("Country"))
        country_code = from_union([from_str, from_none], obj.get("CountryCode"))
        customer_contact = from_union([from_str, from_none], obj.get("CustomerContact"))
        customer_date = from_union([from_str, from_none], obj.get("CustomerDate"))
        customer_group_code = from_union([from_str, from_none], obj.get("CustomerGroupCode"))
        customer_tax_group_code = from_union([from_str, from_none], obj.get("CustomerTaxGroupCode"))
        discount_to_apply = from_union([from_int, from_none], obj.get("DiscountToApply"))
        email = from_union([from_str, from_none], obj.get("Email"))
        export_customer = from_union([from_int, from_none], obj.get("ExportCustomer"))
        fax = from_union([from_str, from_none], obj.get("Fax"))
        fiscal_name = from_union([from_str, from_none], obj.get("FiscalName"))
        fiscal_number = from_union([from_str, from_none], obj.get("FiscalNumber"))
        free_field1 = from_union([from_str, from_none], obj.get("FreeField1"))
        free_field2 = from_union([from_str, from_none], obj.get("FreeField2"))
        free_field3 = from_union([from_str, from_none], obj.get("FreeField3"))
        free_field4 = from_union([from_str, from_none], obj.get("FreeField4"))
        free_field5 = from_union([from_str, from_none], obj.get("FreeField5"))
        full_address = from_union([from_str, from_none], obj.get("FullAddress"))
        is_provider = from_union([from_int, from_none], obj.get("IsProvider"))
        no_active = from_union([from_int, from_none], obj.get("NoActive"))
        no_calculate_service_by_default = from_union([from_int, from_none], obj.get("NoCalculateServiceByDefault"))
        not_show_calculations_in_service_summary_by_default = from_union([from_int, from_none], obj.get("NotShowCalculationsInServiceSummaryByDefault"))
        payment_method_code = from_union([from_str, from_none], obj.get("PaymentMethodCode"))
        postal_code = from_union([from_str, from_none], obj.get("PostalCode"))
        potential_customer = from_union([from_int, from_none], obj.get("PotentialCustomer"))
        price_list_code = from_union([from_str, from_none], obj.get("PriceListCode"))
        print_language = from_union([from_str, from_none], obj.get("PrintLanguage"))
        required_documentation = from_union([from_int, from_none], obj.get("RequiredDocumentation"))
        route_code = from_union([from_str, from_none], obj.get("RouteCode"))
        schipping_method = from_union([from_str, from_none], obj.get("SchippingMethod"))
        special_tax = from_union([from_int, from_none], obj.get("SpecialTax"))
        state_province = from_union([from_str, from_none], obj.get("StateProvince"))
        state_province_code = from_union([from_str, from_none], obj.get("StateProvinceCode"))
        street = from_union([from_str, from_none], obj.get("Street"))
        telephone1 = from_union([from_str, from_none], obj.get("Telephone1"))
        telephone2 = from_union([from_str, from_none], obj.get("Telephone2"))
        terms_of_payment_code = from_union([from_str, from_none], obj.get("TermsOfPaymentCode"))
        terms_of_payment_notes = from_union([from_str, from_none], obj.get("TermsOfPaymentNotes"))
        web = from_union([from_str, from_none], obj.get("Web"))
        return Customer(business_unit_code, code, name, status, account_activity, account_owner_code, account_potentiality, account_size_code, account_state_code, account_type_code, bank_account, bank_name, blocked, carriage_code, city, comments, company_code, country, country_code, customer_contact, customer_date, customer_group_code, customer_tax_group_code, discount_to_apply, email, export_customer, fax, fiscal_name, fiscal_number, free_field1, free_field2, free_field3, free_field4, free_field5, full_address, is_provider, no_active, no_calculate_service_by_default, not_show_calculations_in_service_summary_by_default, payment_method_code, postal_code, potential_customer, price_list_code, print_language, required_documentation, route_code, schipping_method, special_tax, state_province, state_province_code, street, telephone1, telephone2, terms_of_payment_code, terms_of_payment_notes, web)

    def to_dict(self) -> dict:
        result: dict = {}
        result["BusinessUnitCode"] = from_str(self.business_unit_code)
        result["Code"] = from_str(self.code)
        result["Name"] = from_str(self.name)
        result["Status"] = from_int(self.status)
        result["AccountActivity"] = from_union([from_str, from_none], self.account_activity)
        result["AccountOwnerCode"] = from_union([from_str, from_none], self.account_owner_code)
        result["AccountPotentiality"] = from_union([from_int, from_none], self.account_potentiality)
        result["AccountSizeCode"] = from_union([from_str, from_none], self.account_size_code)
        result["AccountStateCode"] = from_union([from_str, from_none], self.account_state_code)
        result["AccountTypeCode"] = from_union([from_str, from_none], self.account_type_code)
        result["BankAccount"] = from_union([from_str, from_none], self.bank_account)
        result["BankName"] = from_union([from_str, from_none], self.bank_name)
        result["Blocked"] = from_union([from_int, from_none], self.blocked)
        result["CarriageCode"] = from_union([from_str, from_none], self.carriage_code)
        result["City"] = from_union([from_str, from_none], self.city)
        result["Comments"] = from_union([from_str, from_none], self.comments)
        result["CompanyCode"] = from_union([from_str, from_none], self.company_code)
        result["Country"] = from_union([from_str, from_none], self.country)
        result["CountryCode"] = from_union([from_str, from_none], self.country_code)
        result["CustomerContact"] = from_union([from_str, from_none], self.customer_contact)
        result["CustomerDate"] = from_union([from_str, from_none], self.customer_date)
        result["CustomerGroupCode"] = from_union([from_str, from_none], self.customer_group_code)
        result["CustomerTaxGroupCode"] = from_union([from_str, from_none], self.customer_tax_group_code)
        result["DiscountToApply"] = from_union([from_int, from_none], self.discount_to_apply)
        result["Email"] = from_union([from_str, from_none], self.email)
        result["ExportCustomer"] = from_union([from_int, from_none], self.export_customer)
        result["Fax"] = from_union([from_str, from_none], self.fax)
        result["FiscalName"] = from_union([from_str, from_none], self.fiscal_name)
        result["FiscalNumber"] = from_union([from_str, from_none], self.fiscal_number)
        result["FreeField1"] = from_union([from_str, from_none], self.free_field1)
        result["FreeField2"] = from_union([from_str, from_none], self.free_field2)
        result["FreeField3"] = from_union([from_str, from_none], self.free_field3)
        result["FreeField4"] = from_union([from_str, from_none], self.free_field4)
        result["FreeField5"] = from_union([from_str, from_none], self.free_field5)
        result["FullAddress"] = from_union([from_str, from_none], self.full_address)
        result["IsProvider"] = from_union([from_int, from_none], self.is_provider)
        result["NoActive"] = from_union([from_int, from_none], self.no_active)
        result["NoCalculateServiceByDefault"] = from_union([from_int, from_none], self.no_calculate_service_by_default)
        result["NotShowCalculationsInServiceSummaryByDefault"] = from_union([from_int, from_none], self.not_show_calculations_in_service_summary_by_default)
        result["PaymentMethodCode"] = from_union([from_str, from_none], self.payment_method_code)
        result["PostalCode"] = from_union([from_str, from_none], self.postal_code)
        result["PotentialCustomer"] = from_union([from_int, from_none], self.potential_customer)
        result["PriceListCode"] = from_union([from_str, from_none], self.price_list_code)
        result["PrintLanguage"] = from_union([from_str, from_none], self.print_language)
        result["RequiredDocumentation"] = from_union([from_int, from_none], self.required_documentation)
        result["RouteCode"] = from_union([from_str, from_none], self.route_code)
        result["SchippingMethod"] = from_union([from_str, from_none], self.schipping_method)
        result["SpecialTax"] = from_union([from_int, from_none], self.special_tax)
        result["StateProvince"] = from_union([from_str, from_none], self.state_province)
        result["StateProvinceCode"] = from_union([from_str, from_none], self.state_province_code)
        result["Street"] = from_union([from_str, from_none], self.street)
        result["Telephone1"] = from_union([from_str, from_none], self.telephone1)
        result["Telephone2"] = from_union([from_str, from_none], self.telephone2)
        result["TermsOfPaymentCode"] = from_union([from_str, from_none], self.terms_of_payment_code)
        result["TermsOfPaymentNotes"] = from_union([from_str, from_none], self.terms_of_payment_notes)
        result["Web"] = from_union([from_str, from_none], self.web)
        return result


def product_group_from_dict(s: Any) -> Customer:
    return Customer.from_dict(s)


def product_group_to_dict(x: Customer) -> Any:
    return to_class(Customer, x)
