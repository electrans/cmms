from dataclasses import dataclass
from typing import Any, TypeVar, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class Response:
    result_status: str
    entity_name: str
    description: str

    @staticmethod
    def from_dict(obj: Any) -> 'Response':
        assert isinstance(obj, dict)
        result_status = from_str(obj.get("ResultStatus"))
        entity_name = from_str(obj.get("EntityName"))
        description = from_str(obj.get("Description"))
        return Response(result_status, entity_name, description)

    def to_dict(self) -> dict:
        result: dict = {}
        result["ResultStatus"] = from_str(self.result_status)
        result["EntityName"] = from_str(self.entity_name)
        result["Description"] = from_str(self.description)
        return result


def response_from_dict(s: Any) -> Response:
    return Response.from_dict(s)


def response_to_dict(x: Response) -> Any:
    return to_class(Response, x)
