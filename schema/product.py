#
# Product Class from Antay Json schema
#
# Status = 11 -> Record active
# Status = 9 -> Record deleted
#

from dataclasses import dataclass
from typing import Optional, Any, TypeVar, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class Product:
    code: str
    name: str
    product_cost: int
    product_type_code: str
    status: int
    bar_code: Optional[str] = None
    cost_code: Optional[str] = None
    free_field1: Optional[str] = None
    free_field2: Optional[str] = None
    free_field3: Optional[str] = None
    free_field4: Optional[str] = None
    free_field5: Optional[str] = None
    no_active: Optional[int] = None
    not_in_md: Optional[int] = None
    picture_path: Optional[str] = None
    price: Optional[int] = None
    product_group_code: Optional[str] = None
    product_sub_group_code: Optional[str] = None
    tax_to_apply: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Product':
        assert isinstance(obj, dict)
        code = from_str(obj.get("Code"))
        name = from_str(obj.get("Name"))
        product_cost = from_int(obj.get("ProductCost"))
        product_type_code = from_str(obj.get("ProductTypeCode"))
        status = from_int(obj.get("Status"))
        bar_code = from_union([from_str, from_none], obj.get("BarCode"))
        cost_code = from_union([from_str, from_none], obj.get("CostCode"))
        free_field1 = from_union([from_str, from_none], obj.get("FreeField1"))
        free_field2 = from_union([from_str, from_none], obj.get("FreeField2"))
        free_field3 = from_union([from_str, from_none], obj.get("FreeField3"))
        free_field4 = from_union([from_str, from_none], obj.get("FreeField4"))
        free_field5 = from_union([from_str, from_none], obj.get("FreeField5"))
        no_active = from_union([from_int, from_none], obj.get("NoActive"))
        not_in_md = from_union([from_int, from_none], obj.get("NotInMD"))
        picture_path = from_union([from_str, from_none], obj.get("PicturePath"))
        price = from_union([from_int, from_none], obj.get("Price"))
        product_group_code = from_union([from_str, from_none], obj.get("ProductGroupCode"))
        product_sub_group_code = from_union([from_str, from_none], obj.get("ProductSubGroupCode"))
        tax_to_apply = from_union([from_int, from_none], obj.get("TaxToApply"))
        return Product(code, name, product_cost, product_type_code, status, bar_code, cost_code, free_field1, free_field2, free_field3, free_field4, free_field5, no_active, not_in_md, picture_path, price, product_group_code, product_sub_group_code, tax_to_apply)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Code"] = from_str(self.code)
        result["Name"] = from_str(self.name)
        result["ProductCost"] = from_int(self.product_cost)
        result["ProductTypeCode"] = from_str(self.product_type_code)
        result["Status"] = from_int(self.status)
        result["BarCode"] = from_union([from_str, from_none], self.bar_code)
        result["CostCode"] = from_union([from_str, from_none], self.cost_code)
        result["FreeField1"] = from_union([from_str, from_none], self.free_field1)
        result["FreeField2"] = from_union([from_str, from_none], self.free_field2)
        result["FreeField3"] = from_union([from_str, from_none], self.free_field3)
        result["FreeField4"] = from_union([from_str, from_none], self.free_field4)
        result["FreeField5"] = from_union([from_str, from_none], self.free_field5)
        result["NoActive"] = from_union([from_int, from_none], self.no_active)
        result["NotInMD"] = from_union([from_int, from_none], self.not_in_md)
        result["PicturePath"] = from_union([from_str, from_none], self.picture_path)
        result["Price"] = from_union([from_int, from_none], self.price)
        result["ProductGroupCode"] = from_union([from_str, from_none], self.product_group_code)
        result["ProductSubGroupCode"] = from_union([from_str, from_none], self.product_sub_group_code)
        result["TaxToApply"] = from_union([from_int, from_none], self.tax_to_apply)
        return result


def product_from_dict(s: Any) -> Product:
    return Product.from_dict(s)


def product_to_dict(x: Product) -> Any:
    return to_class(Product, x)
