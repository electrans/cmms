# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = work_part_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, Any, List, TypeVar, Type, Callable, cast


T = TypeVar("T")


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def is_type(t: Type[T], x: Any) -> T:
    assert isinstance(x, t)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class WorkLine:
    hours: Optional[int] = None
    description: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'WorkLine':
        assert isinstance(obj, dict)
        hours = from_union([from_none, lambda x: int(from_str(x))], obj.get("hours"))
        description = from_union([from_str, from_none], obj.get("description"))
        return WorkLine(hours, description)

    def to_dict(self) -> dict:
        result: dict = {}
        result["hours"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)), lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.hours)
        result["description"] = from_union([from_str, from_none], self.description)
        return result


@dataclass
class WorkPart:
    customer_id: Optional[int] = None
    project_number: Optional[str] = None
    work_lines: Optional[List[WorkLine]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'WorkPart':
        assert isinstance(obj, dict)
        customer_id = from_union([from_none, lambda x: int(from_str(x))], obj.get("customer_id"))
        project_number = from_union([from_str, from_none], obj.get("project_number"))
        work_lines = from_union([lambda x: from_list(WorkLine.from_dict, x), from_none], obj.get("work_lines"))
        return WorkPart(customer_id, project_number, work_lines)

    def to_dict(self) -> dict:
        result: dict = {}
        result["customer_id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)), lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.customer_id)
        result["project_number"] = from_union([from_str, from_none], self.project_number)
        result["work_lines"] = from_union([lambda x: from_list(lambda x: to_class(WorkLine, x), x), from_none], self.work_lines)
        return result


def work_part_from_dict(s: Any) -> WorkPart:
    return WorkPart.from_dict(s)


def work_part_to_dict(x: WorkPart) -> Any:
    return to_class(WorkPart, x)
