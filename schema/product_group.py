#
# ProductGroup Class from Antay Json schema
#
# Status = 11 -> Record active
# Status = 9 -> Record deleted
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = product_group_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Optional, Any, TypeVar, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class ProductGroup:
    code: str
    name: str
    status: int
    order_index: Optional[int] = None
    short_name: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ProductGroup':
        assert isinstance(obj, dict)
        code = from_str(obj.get("Code"))
        name = from_str(obj.get("Name"))
        status = from_int(obj.get("Status"))
        order_index = from_union([from_int, from_none], obj.get("OrderIndex"))
        short_name = from_union([from_str, from_none], obj.get("ShortName"))
        return ProductGroup(code, name, status, order_index, short_name)

    def to_dict(self) -> dict:
        result: dict = {}
        result["Code"] = from_str(self.code)
        result["Name"] = from_str(self.name)
        result["Status"] = from_int(self.status)
        result["OrderIndex"] = from_union([from_int, from_none], self.order_index)
        result["ShortName"] = from_union([from_str, from_none], self.short_name)
        return result


def product_group_from_dict(s: Any) -> ProductGroup:
    return ProductGroup.from_dict(s)


def product_group_to_dict(x: ProductGroup) -> Any:
    return to_class(ProductGroup, x)
