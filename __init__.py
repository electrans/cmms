# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import cron
from . import routes
from . import user

__all__ = ['register', 'routes']

def register():
    Pool.register(
        cron.Cron,
        user.UserApplication,
        module='electrans_cmms', type_='model')
