# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from typing import List

from trytond.i18n import gettext
from trytond.pool import PoolMeta, Pool
from trytond.config import config
from .schema.customer import Customer
from .schema.product import Product
from .schema.product_group import ProductGroup
from .schema.response import Response, response_from_dict
import requests
import json
import logging

_logger = logging.getLogger(__name__)

_HEADERS = {
    'Content-Type': 'application/json; charset=utf-8',
}
# Todo: Del in production
_DEV_ENDPOINT = "https://98dd2a79-9471-4d8b-9553-2ea91ea372a2.mock.pstmn.io"


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super(Cron, cls).__setup__()
        cls.method.selection.extend([
            ('ir.cron|cmms_product_initial_import', 'CMMS Product Initial Import'),
            ('ir.cron|cmms_product_category_initial_import', 'CMMS Product Category Initial Import'),
            ('ir.cron|cmms_customer_initial_import', 'CMMS Customer Initial Import'),
        ])

    @classmethod
    def get_url_from_config(cls):
        try:
            url = config.get('cmms', 'url', default='')
            if not url:
                raise Exception(gettext('electrans_cmms.msg_cmms_ip_port_not_set'))
            return 'http://%s/import' % url
        except Exception as e:
            _logger.error('Error cmss in get_url: %s' % e)

    @classmethod
    def cmms_product_initial_import(cls):
        url = cls.get_url_from_config()
        s_entity_name = 'Product'
        b_allow_modify = 1

        endpoint = '%s/%s/%s' % (url, s_entity_name, b_allow_modify)

        # Get all Product
        products = Pool().get('product.template').search([])
        product_list = []
        try:
            for product in products:
                product_cmms = Product(
                    code=product.code if isinstance(product.code, str) else str(product.code),
                    name=product.name,
                    product_cost=0,  # Todo: check what product cost and convert from Decimal to in
                    product_type_code=product.type,
                    status=11,
                    bar_code='',
                    cost_code='',
                    free_field1='',
                    free_field2='',
                    free_field3='',
                    free_field4='',
                    free_field5='',
                    no_active=0 if product.active else 1,
                    not_in_md=0,  # Todo: ?
                    picture_path='',
                    price=0,  # Todo: ask if sale price?
                    product_group_code=str(product.categories_all[0].id) if product.categories_all else '',
                    product_sub_group_code='',  # Todo: ask subgroups?? in ProductGroup dont exist hierarchy
                    tax_to_apply=0,
                )
                product_list.append(product_cmms)
            # post to CMMS
            resp = requests.post(endpoint, headers=_HEADERS, json=[x.to_dict() for x in product_list])
            cls.logger_cmms_response(resp)
        except Exception as e:
            _logger.error('Error cmms in initial_product_import: %s' % e)

    @classmethod
    def cmms_product_category_initial_import(cls):
        url = cls.get_url_from_config()
        s_entity_name = 'ProductGroup'
        b_allow_modify = 1

        endpoint = '%s/%s/%s' % (url, s_entity_name, b_allow_modify)

        ProductCategory = Pool().get('product.category')
        # Get all Product Category
        try:
            product_group_list = []
            product_categories = ProductCategory.search([])

            order_index = 0  # Todo: Ask Antay if use incremental or ID
            for category in product_categories:
                product_group = ProductGroup(
                    code=str(category.id),
                    name=category.rec_name,
                    short_name='',
                    order_index=order_index,
                    status=11,
                )
                product_group_list.append(product_group)
                order_index += 1
            # post to CMMS
            resp = requests.post(endpoint, headers=_HEADERS, json=[x.to_dict() for x in product_group_list])
            cls.logger_cmms_response(resp)
        except Exception as e:
            _logger.error('Error cmms in initial_product_category_import: %s' % e)

    @classmethod
    def cmms_customer_initial_import(cls):
        url = cls.get_url_from_config()

        s_entity_name = 'Customer'
        b_allow_modify = 1

        endpoint = '%s/%s/%s' % (url, s_entity_name, b_allow_modify)

        Party = Pool().get('party.party')
        # Get all Party
        parties = Party.search([('allowed_for_sales', '=', True)])
        party_list = []
        try:
            for party in parties:
                customer = Customer(
                    business_unit_code=party.code,
                    code=str(party.id),
                    name=party.rec_name,
                    status=11,
                    account_activity='',
                    account_owner_code='',
                    account_potentiality=0,
                    account_size_code='',
                    account_state_code='',
                    account_type_code='',
                    bank_account='',
                    bank_name='',
                    blocked=0,
                    carriage_code='',
                    city=party.addresses[0].city if party.addresses else '',
                    comments=party.comment if party.comment else '',
                    company_code='',
                    country='',
                    country_code='',
                    customer_contact='',
                    customer_date='',
                    customer_group_code='',
                    customer_tax_group_code='',
                    discount_to_apply=0,
                    email=party.email if party.email else '',
                    export_customer=0,
                    fax=party.fax if party.fax else '',
                    fiscal_name=party.full_name if party.full_name else '',
                    fiscal_number=party.identifier_code if party.identifier_code else '',
                    free_field1='',
                    free_field2='',
                    free_field3='',
                    free_field4='',
                    free_field5='',
                    full_address=party.addresses[0].full_address if party.addresses else '',
                    is_provider=0,
                    no_active=0 if party.active else 1,
                    no_calculate_service_by_default=0,
                    not_show_calculations_in_service_summary_by_default=0,
                    payment_method_code='',
                    postal_code=party.addresses[0].zip if party.addresses else '',
                    potential_customer=0,
                    price_list_code='',
                    print_language=party.lang.code if party.lang else '',
                    required_documentation=0,
                    route_code='',
                    schipping_method='',
                    special_tax=0,
                    state_province='',
                    state_province_code='',
                    # state_province=party.addresses[0].subdivision.name if party.addresses else '',
                    # state_province_code=party.addresses[0].subdivision.code if party.addresses else '',
                    street=party.addresses[0].street if party.addresses else '',
                    telephone1=party.phone if party.phone else '',
                    telephone2=party.mobile if party.mobile else '',
                    terms_of_payment_code='',
                    terms_of_payment_notes='',
                    web=party.website if party.website else '',
                )
                party_list.append(customer)
            # post to CMMS
            resp = requests.post(endpoint, headers=_HEADERS, json=[x.to_dict() for x in party_list])
            cls.logger_cmms_response(resp)
        except Exception as e:
            _logger.error('Error cmms in initial_customer_import: %s' % e)

    @classmethod
    def logger_cmms_response(cls, response):
        """
            Check response from CMMS
        :param response:
        :return:
        """
        if response.status_code == 200:
            result = Response.from_dict(response.json())
            _logger.error('CMMS response: %s - %s - %s' % (result.result_status, result.entity_name, result.description ))
        else:
            _logger.error('Error cmms code:%s - %s' % (response.status_code, response.text))
